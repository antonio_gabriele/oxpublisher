﻿using System.Web.Mvc;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Markit.Controllers
{
    [AdminAuthorize]
    public class MarkitController : Controller
    {
        public ActionResult Configure()
        {
            return View("Nop.Plugin.Markit.Views.Markit.Configure");
        }
    }
}

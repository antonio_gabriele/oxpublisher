﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Security;
using Category = Publisher.Core.Model.Category;

namespace Nop.Plugin.Markit
{
    public partial class NopService
    {
        /*
        public void DeleteProduct(string usernameOrEmail, string userPassword, Product product)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageProducts))
                throw new ApplicationException("Not allowed to manage products");

            _productService.DeleteProduct(product);
        }

        public Product GetProductById(string usernameOrEmail, string userPassword, int productId)
        {
            return _productService.GetProductById(productId);
        }

        public void InsertProduct(string usernameOrEmail, string userPassword, Product product)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageProducts))
                throw new ApplicationException("Not allowed to manage products");

            _productService.InsertProduct(product);
        }

        public void UpdateProduct(string usernameOrEmail, string userPassword, Product product)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageProducts))
                throw new ApplicationException("Not allowed to manage products");

            _productService.UpdateProduct(product);
        }

        public IList<Product> SearchProducts(string usernameOrEmail, string userPassword, int pageIndex = 0, int pageSize = Int32.MaxValue, IList<int> categoryIds = null, int manufacturerId = 0, int storeId = 0, int vendorId = 0, int warehouseId = 0, int parentGroupedProductId = 0, ProductType? productType = null, bool visibleIndividuallyOnly = false, bool? featuredProducts = null, decimal? priceMin = null, decimal? priceMax = null, int productTagId = 0, string keywords = null, bool searchDescriptions = false, bool searchSku = true, bool searchProductTags = false, int languageId = 0, IList<int> filteredSpecs = null, ProductSortingEnum orderBy = ProductSortingEnum.Position, bool showHidden = false)
        {
            return _productService.SearchProducts(pageIndex, pageSize, categoryIds, manufacturerId, storeId, vendorId, warehouseId, parentGroupedProductId, productType, visibleIndividuallyOnly, featuredProducts, priceMin, priceMax, productTagId, keywords, searchDescriptions, searchSku, searchProductTags, languageId, filteredSpecs, orderBy, showHidden);
        }

        public void DeleteProductPicture(string usernameOrEmail, string userPassword, ProductPicture productPicture)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageProducts))
                throw new ApplicationException("Not allowed to manage products");

            _productService.DeleteProductPicture(productPicture);
        }

        public IList<ProductPicture> GetProductPicturesByProductId(string usernameOrEmail, string userPassword, int productId)
        {
            return _productService.GetProductPicturesByProductId(productId);
        }

        public ProductPicture GetProductPictureById(string usernameOrEmail, string userPassword, int productPictureId)
        {
            return _productService.GetProductPictureById(productPictureId);
        }

        public void InsertProductPicture(string usernameOrEmail, string userPassword, ProductPicture productPicture)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageProducts))
                throw new ApplicationException("Not allowed to manage products");
            _productService.InsertProductPicture(productPicture);
        }

        public void UpdateProductPicture(string usernameOrEmail, string userPassword, ProductPicture productPicture)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageProducts))
                throw new ApplicationException("Not allowed to manage products");

            _productService.UpdateProductPicture(productPicture);
        }

        public void DeleteCategory(string usernameOrEmail, string userPassword, Category category)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageCategories))
                throw new ApplicationException("Not allowed to manage products");

            _categoryService.DeleteCategory(category);
        }
        */
        public List<Category> GetAllCategories(string usernameOrEmail, string userPassword)
        {
            var datas = _categoryService.GetAllCategories().Select(it => new Category()
            {
                Id = it.Id, Caption = it.Name
            }).ToList();
            return datas;
        }

        public Category GetCategoryById(string usernameOrEmail, string userPassword, int categoryId)
        {
            var data = _categoryService.GetCategoryById(categoryId);
            return new Category()
            {
                Id = data.Id,
                Caption = data.Name
            };
        }
        /*
        public void InsertCategory(string usernameOrEmail, string userPassword, Category category)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageCategories))
                throw new ApplicationException("Not allowed to manage products");

            _categoryService.InsertCategory(category);
        }

        public void UpdateCategory(string usernameOrEmail, string userPassword, OneCore.Model.Category category)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageCategories))
                throw new ApplicationException("Not allowed to manage products");

            _categoryService.UpdateCategory(category);
        }

        public void DeleteProductCategory(string usernameOrEmail, string userPassword, ProductCategory productCategory)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageCategories))
                throw new ApplicationException("Not allowed to manage products");

            _categoryService.DeleteProductCategory(productCategory);
        }

        public void InsertProductCategory(string usernameOrEmail, string userPassword, ProductCategory productCategory)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageCategories))
                throw new ApplicationException("Not allowed to manage products");

            _categoryService.InsertProductCategory(productCategory);
        }

        public void UpdateProductCategory(string usernameOrEmail, string userPassword, ProductCategory productCategory)
        {
            CheckAccess(usernameOrEmail, userPassword);
            if (!_permissionSettings.Authorize(StandardPermissionProvider.ManageCategories))
                throw new ApplicationException("Not allowed to manage products");

            _categoryService.UpdateProductCategory(productCategory);
        }
        */
    }
}

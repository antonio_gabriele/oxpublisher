﻿//Contributor: Nicolas Muniere

using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Category = Publisher.Core.Model.Category;

namespace Nop.Plugin.Markit
{

    [ServiceContract]
    public interface INopService
    {
        /*
        [OperationContract]
        void DeleteProduct(string usernameOrEmail, string userPassword, Product product);
        [OperationContract]
        Product GetProductById(string usernameOrEmail, string userPassword, int productId);
        [OperationContract]
        void InsertProduct(string usernameOrEmail, string userPassword, Product product);
        [OperationContract]
        void UpdateProduct(string usernameOrEmail, string userPassword, Product product);
        [OperationContract]
        IList<Product> SearchProducts(string usernameOrEmail, string userPassword, int pageIndex = 0, int pageSize = Int32.MaxValue, IList<int> categoryIds = null, int manufacturerId = 0, int storeId = 0, int vendorId = 0, int warehouseId = 0, int parentGroupedProductId = 0, ProductType? productType = null, bool visibleIndividuallyOnly = false, bool? featuredProducts = null, decimal? priceMin = null, decimal? priceMax = null, int productTagId = 0, string keywords = null, bool searchDescriptions = false, bool searchSku = true, bool searchProductTags = false, int languageId = 0, IList<int> filteredSpecs = null, ProductSortingEnum orderBy = ProductSortingEnum.Position, bool showHidden = false);
        [OperationContract]
        void DeleteProductPicture(string usernameOrEmail, string userPassword, ProductPicture productPicture);
        [OperationContract]
        IList<ProductPicture> GetProductPicturesByProductId(string usernameOrEmail, string userPassword, int productId);
        [OperationContract]
        ProductPicture GetProductPictureById(string usernameOrEmail, string userPassword, int productPictureId);
        [OperationContract]
        void InsertProductPicture(string usernameOrEmail, string userPassword, ProductPicture productPicture);
        [OperationContract]
        void UpdateProductPicture(string usernameOrEmail, string userPassword, ProductPicture productPicture);
        [OperationContract]
        void DeleteCategory(string usernameOrEmail, string userPassword, OneCore.Model.Category category);
        */
        [OperationContract]
        List<Category> GetAllCategories(string usernameOrEmail, string userPassword);
        [OperationContract]
        Category GetCategoryById(string usernameOrEmail, string userPassword, int categoryId);
        /*
        [OperationContract]
        void InsertCategory(string usernameOrEmail, string userPassword, OneCore.Model.Category category);
        [OperationContract]
        void UpdateCategory(string usernameOrEmail, string userPassword, OneCore.Model.Category category);
        [OperationContract]
        void DeleteProductCategory(string usernameOrEmail, string userPassword, ProductCategory productCategory);
        [OperationContract]
        void InsertProductCategory(string usernameOrEmail, string userPassword, ProductCategory productCategory);
        [OperationContract]
        void UpdateProductCategory(string usernameOrEmail, string userPassword, ProductCategory productCategory);
        */
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Publisher.Browser.UI
{
    public class WebexBrowser : WebBrowser, INotifyPropertyChanged
    {
        private Action<string> capturedImage;
        private Action<string> capturedText;
        /**/
        public enum Mode
        {
            None, Text, Image
        }

        public bool Free()
        {
            return this.mode == Mode.None;
        }

        public bool Busy()
        {
            return this.mode != Mode.None;
        }

        public void CaptureText(Action<string> act)
        {
            this.capturedText = act;
            this.mode = Mode.Text;
            this.AddHandler();            
        }

        public void CaptureImage(Action<string> act)
        {
            this.capturedImage = act;
            this.mode = Mode.Image;
            this.AddHandler();
        }

        public void CaptureNone()
        {
            this.mode = Mode.None;
            this.RemoveHandler();
        }

        private string Url;

        public WebexBrowser()
        {
            this.NewWindow += (sender, args) =>
            {
                args.Cancel = true;
                if (this.StatusText.StartsWith("http"))
                    this.Navigate(this.StatusText);
            };
            this.ScriptErrorsSuppressed = true;
        }
        
        private DateTime tick;

        public Mode mode { get; set; }

        private readonly IDictionary<HtmlElement, String> status = new Dictionary<HtmlElement, string>();

        private void CleanStyle()
        {
            foreach (var statu in this.status)
            {
                statu.Key.Style = statu.Value;
            }
            this.status.Clear();
        }

        private Point lastClientMousePosition;

        private bool AlreadyExecuted(Point clientMousePosition)
        {
            if (this.lastClientMousePosition.X == clientMousePosition.X && this.lastClientMousePosition.Y == clientMousePosition.Y)
            {
                return true;
            }
            this.lastClientMousePosition = clientMousePosition;
            return false;
        }

        private void DocumentOnMouseOver(object sender, HtmlElementEventArgs htmlElementEventArgs)
        {
            var document = (HtmlDocument)sender;
            var node = document.GetElementFromPoint(htmlElementEventArgs.ClientMousePosition);
            if (node == null)
            {
                return;
            }
            if (node.TagName.ToLower() == "frame" || node.TagName.ToLower() == "iframe")
            {
                return;
            }
            if (this.AlreadyExecuted(htmlElementEventArgs.ClientMousePosition))
            {
                return;
            }
            this.CleanStyle();
            if ((node.TagName.ToLower() == "body")
                || (node.TagName.ToLower() == "html"))
            {
                return;
            }
            if (this.mode == Mode.None)
            {
                return;
            }
            switch (this.mode)
            {
                case Mode.Image:
                    if (node.TagName.ToLower() != "img")
                    {
                        return;
                    }
                    break;
                case Mode.Text:
                    if (node.TagName.ToLower() == "img")
                    {
                        return;
                    }
                    if (this.capturedText != null)
                    {
                        this.capturedText(node.InnerText);
                    }
                    break;
            }
            if (this.status.ContainsKey(node) == false)
            {
                this.status.Add(node, node.Style);
            }
            node.Style = String.Concat(this.mode == Mode.Image ? "/**/opacity:0.4;filter:alpha(opacity=40);/**/" : "/**/background-color: darkgray; color: white;/**/", node.Style);
        }

        public void AddHandler()
        {
            this.AllowNavigation = false;
            if (this.Document == null)
            {
                return;
            }
            this.Document.MouseDown += this.DocumentOnMouseDown;
            this.Document.MouseOver += this.DocumentOnMouseOver;
            if (this.Document.Window == null || this.Document.Window.Frames == null)
            {
                return;
            }
            try
            {
                foreach (HtmlWindow window in this.Document.Window.Frames)
                {
                    window.Document.MouseDown += this.DocumentOnMouseDown;
                    window.Document.MouseOver += this.DocumentOnMouseOver;
                }
            }
            catch (Exception)
            {

                foreach (HtmlElement frame in this.Document.GetElementsByTagName("iframe"))
                {
                    frame.Document.MouseDown += this.DocumentOnMouseDown;
                    frame.Document.MouseOver += this.DocumentOnMouseOver;
                }
                foreach (HtmlElement frame in this.Document.GetElementsByTagName("frame"))
                {
                    frame.Document.MouseDown += this.DocumentOnMouseDown;
                    frame.Document.MouseOver += this.DocumentOnMouseOver;
                }
            }
        }

        //private string lastImage;

        private void DocumentOnMouseDown(object sender, HtmlElementEventArgs htmlElementEventArgs)
        {
            if (this.AlreadyExecuted(htmlElementEventArgs.ClientMousePosition))
            {
                return;
            }
            var document = (HtmlDocument)sender;
            var node = document.GetElementFromPoint(htmlElementEventArgs.ClientMousePosition);
            if (node == null)
            {
                return;
            }
            switch (this.mode)
            {
                case Mode.Image:
                    this.tick = DateTime.Now;
                    if (node == null)
                    {
                        break;
                    }
                    if (node.TagName.ToLower() == "img")
                    {
                        var src = node.GetAttribute("src");
                        if (string.IsNullOrWhiteSpace(src))
                        {
                            this.mode = Mode.None;
                            return;
                        }
                        this.mode = Mode.None;
                        if (this.capturedImage != null)
                        {
                            this.capturedImage(src);
                        }
                        this.CaptureNone();
                    }
                    break;
                case Mode.Text:
                    this.tick = DateTime.Now;
                    this.mode = Mode.None;
                    this.CaptureNone();
                    break;
            }
        }

        public void RemoveHandler()
        {
            this.AllowNavigation = true;
            if (this.Document == null)
            {
                return;
            }
            this.Document.MouseDown -= this.DocumentOnMouseDown;
            this.Document.MouseOver -= this.DocumentOnMouseOver;
            if (this.Document.Window == null || this.Document.Window.Frames == null)
            {
                return;
            }
            try
            {
                foreach (HtmlWindow window in this.Document.Window.Frames)
                {
                    window.Document.MouseDown -= this.DocumentOnMouseDown;
                    window.Document.MouseOver -= this.DocumentOnMouseOver;

                }
            }
            catch (Exception)
            {
                foreach (HtmlElement frame in this.Document.GetElementsByTagName("iframe"))
                {
                    frame.Document.Window.Document.MouseDown -= this.DocumentOnMouseDown;
                    frame.Document.Window.Document.MouseOver -= this.DocumentOnMouseOver;
                }
                foreach (HtmlElement frame in this.Document.GetElementsByTagName("frame"))
                {
                    frame.Document.Window.Document.MouseDown -= this.DocumentOnMouseDown;
                    frame.Document.Window.Document.MouseOver -= this.DocumentOnMouseOver;
                }
            }
            this.CleanStyle();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

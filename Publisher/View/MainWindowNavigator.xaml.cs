﻿using System.Windows.Controls;

namespace Publisher.View
{
    /// <summary>
    /// Interaction logic for MainWindowNavigator.xaml
    /// </summary>
    public partial class MainWindowNavigator : UserControl
    {
        public MainWindowNavigator()
        {
            this.InitializeComponent();
        }
    }
}

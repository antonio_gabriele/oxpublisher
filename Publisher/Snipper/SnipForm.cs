using System;
using System.Drawing;
using System.Windows.Forms;

namespace Publisher.Snipper
{
    public partial class SnipForm : Form
    {
        public delegate void SnipHandler(Image image);

        public event SnipHandler Snip;

        public bool MouseButtonDown = false;
        public bool RectangleDrawn = false;

        public Point ClickPoint = new Point();
        public Point CurrentTopLeft = new Point();
        public Point CurrentBottomRight = new Point();
        public Point VirtualScrOrigin = new Point();

        readonly Graphics graphics;
        readonly Pen selectionPen = new Pen(Color.Red, 3);
        readonly SolidBrush transparentBrush = new SolidBrush(Color.White);
        readonly Pen eraserPen = new Pen(Color.FromArgb(192, 192, 255), 3);
        readonly SolidBrush eraserBrush = new SolidBrush(Color.FromArgb(192, 192, 255));

        public SnipForm()
        {
            this.InitializeComponent();
            this.MouseDown += this.mouse_Down;
            this.MouseUp += this.mouse_Up;
            this.MouseMove += this.mouse_Move;
            this.KeyUp += this.key_press;
            this.MouseClick += this.SnipForm_MouseClick;
            this.Bounds = new Rectangle(SystemInformation.VirtualScreen.X, SystemInformation.VirtualScreen.Y, SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
            this.VirtualScrOrigin.X = SystemInformation.VirtualScreen.X;
            this.VirtualScrOrigin.Y = SystemInformation.VirtualScreen.Y;
            this.graphics = this.CreateGraphics();
            
        }

        public void key_press(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape){
                this.Close();
            }
        }

        void SnipForm_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle){
                this.Close();
            }
        }

        private void mouse_Down(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right) {
                this.MouseButtonDown = true;
                this.ClickPoint = new Point(MousePosition.X, MousePosition.Y);
                this.CurrentTopLeft.X = this.CurrentBottomRight.X = this.ClickPoint.X;
                this.CurrentTopLeft.Y = this.CurrentBottomRight.Y = this.ClickPoint.Y;
            }
        }

        private void mouse_Up(object sender, MouseEventArgs e)
        {
            this.MouseButtonDown = false;
            if (e.Button == MouseButtons.Left) this.SaveSelection(false, false, false);
            if (e.Button == MouseButtons.Right) this.SaveSelection(false, true, false);
        }

        private void mouse_Move(object sender, MouseEventArgs e)
        {
            if (this.MouseButtonDown && !this.RectangleDrawn){
                this.DrawSelection();
            }
        }

        private void DrawSelection()
        {
            this.Cursor = Cursors.Arrow;
            this.graphics.DrawRectangle(this.eraserPen, this.CurrentTopLeft.X - this.VirtualScrOrigin.X, this.CurrentTopLeft.Y - this.VirtualScrOrigin.Y, this.CurrentBottomRight.X - this.CurrentTopLeft.X, this.CurrentBottomRight.Y - this.CurrentTopLeft.Y);
            this.graphics.FillRectangle(this.eraserBrush, this.CurrentTopLeft.X - this.VirtualScrOrigin.X, this.CurrentTopLeft.Y - this.VirtualScrOrigin.Y, this.CurrentBottomRight.X - this.CurrentTopLeft.X, this.CurrentBottomRight.Y - this.CurrentTopLeft.Y);
            this.CurrentTopLeft.X = ((Cursor.Position.X < this.ClickPoint.X) ? Cursor.Position.X : this.ClickPoint.X);
            this.CurrentBottomRight.X = ((Cursor.Position.X < this.ClickPoint.X) ? this.ClickPoint.X : Cursor.Position.X);
            this.CurrentTopLeft.Y = ((Cursor.Position.Y < this.ClickPoint.Y) ? Cursor.Position.Y : this.ClickPoint.Y);
            this.CurrentBottomRight.Y = ((Cursor.Position.Y < this.ClickPoint.Y) ? this.ClickPoint.Y : Cursor.Position.Y);
            this.graphics.DrawRectangle(this.selectionPen, this.CurrentTopLeft.X - this.VirtualScrOrigin.X, this.CurrentTopLeft.Y - this.VirtualScrOrigin.Y, this.CurrentBottomRight.X - this.CurrentTopLeft.X, this.CurrentBottomRight.Y - this.CurrentTopLeft.Y);
            this.graphics.FillRectangle(this.transparentBrush, this.CurrentTopLeft.X - this.VirtualScrOrigin.X, this.CurrentTopLeft.Y - this.VirtualScrOrigin.Y, this.CurrentBottomRight.X - this.CurrentTopLeft.X, this.CurrentBottomRight.Y - this.CurrentTopLeft.Y);
        }


        public void SaveSelection(bool showCursor, bool chooseLocation, bool fullscreen)
        {
            var startPoint = new Point(0, 0);
            var bounds = new Rectangle(startPoint, new Size(1200, 700));
            if (!fullscreen){
                startPoint = new Point(this.CurrentTopLeft.X, this.CurrentTopLeft.Y);
                bounds = new Rectangle(this.CurrentTopLeft.X, this.CurrentTopLeft.Y, this.CurrentBottomRight.X - this.CurrentTopLeft.X, this.CurrentBottomRight.Y - this.CurrentTopLeft.Y);
            }
            if (bounds.Height == 0 || bounds.Width == 0) {
                this.Close(); return; }
            var bitmap = new Bitmap(bounds.Width, bounds.Height);
            var g = Graphics.FromImage(bitmap);
            g.CopyFromScreen(startPoint, Point.Empty, bounds.Size);
            this.Snip(bitmap);
            this.Close();
        }

        private void SnipForm_Load(object sender, EventArgs e)
        {
            this.Location = new Point(SystemInformation.VirtualScreen.X,SystemInformation.VirtualScreen.Y);
            this.Size = new Size(SystemInformation.VirtualScreen.Width,SystemInformation.VirtualScreen.Height);
        }
    }
}
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace Publisher.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<MainOneClickViewModel>();
            SimpleIoc.Default.Register<MainNopViewModel>();
            SimpleIoc.Default.Register<MainImportViewModel>();
            SimpleIoc.Default.Register<MainNavigatorViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<CreateCategoryViewModel>();
            SimpleIoc.Default.Register<RTFEditorModel>();
        }

        public CreateCategoryViewModel CreateCategory
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CreateCategoryViewModel>();
            }
        }

        public RTFEditorModel RTFEditor
        {
            get
            {
                return ServiceLocator.Current.GetInstance<RTFEditorModel>();
            }
        }

        public MainNopViewModel MainNop
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainNopViewModel>();
            }
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public MainOneClickViewModel MainOneClick
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainOneClickViewModel>();
            }
        }

        public MainImportViewModel MainImport
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainImportViewModel>();
            }
        }

        public MainNavigatorViewModel MainNavigator
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainNavigatorViewModel>();
            }
        }

        public LoginViewModel Login
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LoginViewModel>();
            }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}
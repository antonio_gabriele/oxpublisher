using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Data;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using Bootstrap;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using Publisher.Browser.UI;
using Publisher.Core.Model;
using Publisher.Framework;
using Publisher.Framework.Connectors.Facebook;
using Publisher.Framework.Connectors.Mark;
using Publisher.Framework.Connectors.Twitter;
using Publisher.Framework.UserControl;
using Publisher.Snipper;

namespace Publisher.ViewModel
{
    public class MainNavigatorViewModel : ViewModelBase
    {
        public enum Mode
        {
            None,
            Nuovo,
            Modifica
        }

        public ICommand CommandAnnulla { get; private set; }
        public ICommand CommandWebBrowserLoaded { get; private set; }
        public ICommand CommandGo { get; private set; }
        public ICommand CommandCarica { get; private set; }
        public ICommand CommandCrop { get; private set; }
        public ICommand CommandZoom { get; private set; }
        public ICommand CommandSalva { get; private set; }
        public ICommand CommandBack { get; private set; }
        public ICommand CommandClose { get; private set; }
        public ICommand CommandNuovo { get; private set; }
        public ICommand CommandFoto { get; private set; }
        public ICommand CommandCategoriaCrea { get; set; }
        public ICommand CommandDescrizioneNuova { get; private set; }
        public ICommand CommandDescrizioneEditor { get; private set; }
        public ICommand CommandDescrizioneBreveNuova { get; private set; }
        public ICommand CommandDescrizioneBreveEditor { get; private set; }
        public ICommand CommandDescrizioneAppend { get; private set; }
        public ICommand CommandDescrizioneBreveAppend { get; private set; }
        public ICommand CommandWebBrowserLoadCompleted { get; private set; }
        public ICommand CommandImageNuova { get; private set; }
        public ICommand CommandImageElimina { get; private set; }
        /**/
        public OxItem Item { get; set; }
        private readonly OxItem backup = new OxItem();
        public bool ShowZoom { get; set; }
        public Mode mode { get; set; }
        public String SourceAddress { get; set; }
        public OxImage Picture { get; set; }
        /**/
        private WebexBrowser browser;
        /**/
        private readonly ObservableCollection<OxCategory> categorie = new ObservableCollection<OxCategory>();
        /**/
        public ListCollectionView Categorie { get; set; }

        private void Acquire(Image image)
        {
            Extension.Run(() =>
            {
                var scale = image.GetThumbnailImageX();
                var file = Extension.PictureTempName();
                scale.Save(file, ImageFormat.Png);
                this.Picture.Modified = true;
                this.Picture.DefaultPictureURL = file;
                this.RaisePropertyChanged(() => this.Picture);
            });
        }

        private void RegisterMessage()
        {
            Messenger.Default.Register<Message>(this, Message.Type.PrepareNew, (message) => this.CommandNuovo.Execute());
            Messenger.Default.Register<Message>(this, Message.Type.ReloadLookup, (message) => this.ReloadLookup());
            Messenger.Default.Register<OxItem>(this, Message.Type.OpenItem, (item) =>
            {
                if (this.browser.Busy())
                {
                    this.CommandAnnulla.Execute();
                }
                item.CopyTo(this.Item);
                item.CopyTo(this.backup);
                this.SourceAddress = string.Format("https://www.google.it/search?q={0}", this.Item.Caption);
                this.CommandGo.Execute(null);
                this.browser.Focus();
                this.mode = Mode.Modifica;
            });
        }

        public MainNavigatorViewModel()
        {
            this.RegisterMessage();
            this.Item = new OxItem();
            this.backup = new OxItem();
            this.Categorie = new ListCollectionView(this.categorie);
            this.CommandImageElimina = new RelayCommand(() =>
            {
                this.Picture.Deleted = true;
                this.Picture.Modified = false;
                this.Item.OxImage.Remove(this.Picture);
            }, () => this.browser != null && this.browser.Free() && this.Picture != null);
            this.CommandImageNuova = new RelayCommand(() =>
            {
                int ident = 0;
                if (this.Item.OxImage.Any())
                    ident = this.Item.OxImage.Max(it => it.Index) + 1;
                var picture = new OxImage();
                picture.Index = ident;
                picture.Name = Extension.PictureName(picture.Index);
                this.Item.OxImage.Add(picture);
                this.Picture = picture;
            }, () => this.mode == Mode.Nuovo || this.mode == Mode.Modifica);
            this.CommandGo = new RelayCommand(this.Go, () => String.IsNullOrWhiteSpace(this.SourceAddress) == false);
            this.CommandCrop = new RelayCommand(() =>
            {
                var form = new SnipForm();
                form.Snip += this.Acquire;
                form.Show();
            }, () => this.browser != null && this.browser.Free() && this.Picture != null);
            this.CommandCarica = new RelayCommand(() =>
            {
                var dialog = new Microsoft.Win32.OpenFileDialog();
                dialog.DefaultExt = ".*";
                dialog.Filter = "All Supported Types|*.bmp;*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.gif;*.pdf;";
                var result = dialog.ShowDialog();
                if (result == true)
                {
                    this.browser.Navigate(dialog.FileName);
                }
            }, () => this.browser != null && this.browser.Free() && this.Picture != null);
            this.CommandAnnulla = new RelayCommand(() =>
            {
                if (this.browser.Free())
                {
                    this.Item.Caption = this.Item.Description = string.Empty;
                    this.Item.Price = 0;
                    this.Item.OriginalPrice = null;
                    this.Item.OxImage =  new ObservableCollection<OxImage>();
                    this.mode = Mode.None;
                    this.RaisePropertyChanged(() => this.Picture);
                }
                else
                {
                    this.backup.CopyTo(this.Item);
                    this.browser.CaptureNone();
                    this.RaisePropertyChanged(() => this.Picture);
                }
            }, () => this.browser != null && (this.mode != Mode.None || this.browser.Busy()));
            this.CommandDescrizioneNuova = new RelayCommand(() =>
            {
                this.Item.CopyTo(this.backup);
                this.browser.CaptureText((text) =>
                {
                    this.Item.Description = text;
                });
            }, () => this.browser != null && this.browser.Free());
            this.CommandDescrizioneEditor = new RelayCommand(() =>
            {
                this.Item.CopyTo(this.backup);
                var message = new Message<string, string>();
                message.Data = this.Item.Caption;
                message.Exit = (msg) =>
                {
                    this.Item.Caption = msg;
                };
                Messenger.Default.Send(message, Message.Type.OpenEditor);
            }, () => this.browser != null && this.browser.Free());
            this.CommandDescrizioneAppend = new RelayCommand(() =>
            {
                this.Item.CopyTo(this.backup);
                this.browser.CaptureText((text) =>
                {
                    this.Item.Description = this.backup.Description + " " + text;
                });
            }, () => this.browser != null && this.browser.Free());
            this.CommandDescrizioneBreveNuova = new RelayCommand(() =>
            {
                this.Item.CopyTo(this.backup);
                this.browser.CaptureText((text) =>
                {
                    this.Item.Caption = text;
                });
            }, () => this.browser != null && this.browser.Free());
            this.CommandDescrizioneBreveEditor = new RelayCommand(() =>
            {
                this.Item.CopyTo(this.backup);
                var message = new Message<string, string>();
                message.Data = this.Item.Caption;
                message.Exit = (msg) =>
                {
                    this.Item.Caption = msg;
                };
                Messenger.Default.Send(message, Message.Type.OpenEditor);
            }, () => this.browser != null && this.browser.Free());
            this.CommandDescrizioneBreveAppend = new RelayCommand(() =>
            {                
                this.Item.CopyTo(this.backup);
                this.browser.CaptureText((text) =>
                {
                    this.Item.Caption = this.backup.Caption + " " + text;
                });
            }, () => this.browser != null && this.browser.Free());
            this.CommandFoto = new RelayCommand(() =>
            {
                this.Item.CopyTo(this.backup);
                this.browser.CaptureImage((src) => this.Acquire(Extension.LoadImageFromIMGTag(src, this.browser.Url.AbsolutePath)));
            }, () => this.browser != null && this.browser.Free());
            this.CommandBack = new RelayCommand(() => this.browser.GoBack());
            this.CommandCategoriaCrea = new RelayCommand(() => Messenger.Default.Send(new Message(), Message.Type.OpenCreateCategory),
                                                    () => this.browser != null && this.browser.Free());
            this.CommandSalva = new RelayCommand(() => Extension.Run(() =>
            {
                try
                {
                    var saved = MarkOffersConnector.Instance.SetArticolo(this.Item);
                    FacebookConnector.Instance.Post(saved);
                    TwitterConnector.Instance.Post(saved);
                }
                catch (Exception e)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(e.Message);
                }
                finally
                {
                    DispatcherHelper.CheckBeginInvokeOnUI(() =>
                    {
                        this.CommandAnnulla.Execute();
                        this.CommandNuovo.Execute();
                    });                        
                }
            }),
            () => this.mode != Mode.None
                    && this.Item != null
                    && string.IsNullOrWhiteSpace(this.Item.Caption) == false
                    && string.IsNullOrWhiteSpace(this.Item.Description) == false
                    && string.IsNullOrWhiteSpace(this.Item.Kind) == false);
            this.CommandZoom = new RelayCommand(() =>
            {
                this.ShowZoom = !this.ShowZoom;
            });
            this.CommandNuovo = new RelayCommand(() =>
            {
                this.Item.Id = 0;
                this.Item.Caption = string.Empty;
                this.Item.Description = string.Empty;
                this.mode = Mode.Nuovo;
                this.CommandImageNuova.Execute(null);
            }, () => this.mode == Mode.None);
            this.CommandWebBrowserLoaded = new RelayCommand<WindowsFormsHost>((host) =>
            {
                this.browser = (WebexBrowser) host.Child;
                this.browser.Navigated += (sender, args) =>
                {
                    this.SourceAddress = this.browser.Url.ToString();
                };
            });
            this.ReloadLookup();
        }

        private void ReloadLookup()
        {
            this.categorie.Clear();
            this.categorie.Insert(0, new OxCategory());
            MarkOffersConnector.Instance.GetCategorie((result) => result.ForEach(this.categorie.Add));
        }

        private void Go()
        {
            this.browser.CaptureNone();
            if (this.SourceAddress.StartsWith("http") == false && this.SourceAddress.StartsWith("https") == false)
            {
                this.SourceAddress = string.Format("http://{0}", this.SourceAddress);
            }
            this.browser.Navigate(this.SourceAddress);
        }
    }
}
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using Publisher.Core.Model;
using Publisher.Framework;

namespace Publisher.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public int Tab { get; set; }

        public bool Busy { get; set; }

        public MainViewModel()
        {
            Messenger.Default.Register<OxItem>(this, Message.Type.OpenItem, (item) => Extension.Run(() =>
            {
                this.Tab = 0;
            }));
            Messenger.Default.Register<Message>(this, Message.Type.Busy, (item) => DispatcherHelper.CheckBeginInvokeOnUI(() => this.Busy = true));
            Messenger.Default.Register<Message>(this, Message.Type.Ready, (item) => DispatcherHelper.CheckBeginInvokeOnUI(() => this.Busy = false));
        }
    }
}
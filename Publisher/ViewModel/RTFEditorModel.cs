using System;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Publisher.ViewModel
{
    public class RTFEditorModel : ViewModelBase
    {
        public RTFEditorModel()
        {
            Document = new FlowDocument();
            this.CommandChiudi = new RelayCommand<Window>((window) => this.Exit(this.Data));
        }
        public FlowDocument Document { get; set; }
        public ICommand CommandChiudi { get; private set; }
        public Action<string> Exit { get; set; }
        public string Data { get; set; }
    }
}
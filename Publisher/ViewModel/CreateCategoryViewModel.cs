using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Publisher.Framework;
using Publisher.Framework.Connectors.Mark;

namespace Publisher.ViewModel
{
    public class CreateCategoryViewModel : ViewModelBase
    {
        public CreateCategoryViewModel()
        {
            this.CommandCrea = new RelayCommand<Window>((window) =>
            {
                MarkOffersConnector.Instance.CreateCategory(this.Caption);
                window.Close();
                MarkOffersConnector.Instance.Configure();
                Messenger.Default.Send(new Message(), Message.Type.ReloadLookup);
            }, (window) => string.IsNullOrWhiteSpace(this.Caption) == false);
            this.CommandChiudi = new RelayCommand<Window>((window) => window.Close());
        }
        public ICommand CommandCrea { get; private set; }
        public ICommand CommandChiudi { get; private set; }

        public string Caption { get; set; }
    }
}
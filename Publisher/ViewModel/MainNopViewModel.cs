using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;
using Bootstrap;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Publisher.Core.Model;
using Publisher.Framework;
using Publisher.Framework.Connectors.Facebook;
using Publisher.Framework.Connectors.Mark;
using Publisher.Framework.Connectors.Twitter;
using Publisher.MarkOffers;

namespace Publisher.ViewModel
{
    public class MainNopViewModel : ViewModelBase
    {
        public ICommand CmdSearch { get; private set; }
        public ICommand CommandAvanti { get; private set; }
        public ICommand CommandIndietro { get; private set; }
        public ICommand CmdItemOpen { get; private set; }
        public ICommand CmdItemDelete { get; private set; }
        public ICommand CmdPost { get; private set; }
        public ObservableCollection<MO_SERVICE> SelectedServices { get; set; }
        /**/
        private readonly ObservableCollection<OxCategory> categorie = new ObservableCollection<OxCategory>();
        private readonly ObservableCollection<OxItem> articles = new ObservableCollection<OxItem>();
        /**/
        public ListCollectionView Articles { get; set; }
        public ListCollectionView Categorie { get; set; }
        /**/
        public MarkQuery Query { get; set; }
        /**/
        public MainNopViewModel()
        {
            Messenger.Default.Register<Message>(this, Message.Type.ReloadLookup, (message) => this.ReloadLookup());
            this.SelectedServices = new ObservableCollection<MO_SERVICE>();
            this.Categorie = new ListCollectionView(this.categorie);
            this.Articles = new ListCollectionView(this.articles);
            this.Query = new MarkQuery();
            this.CmdSearch = new RelayCommand(() =>
            {
                this.articles.Clear();
                MarkOffersConnector.Instance.GetArticolo(this.Query, it =>  it.ForEach(this.articles.Add));
            });
            this.CmdItemDelete = new RelayCommand(() =>
            {
                var item = (OxItem)this.Articles.CurrentItem;
                MarkOffersConnector.Instance.Delete(item);
                this.Articles.Remove(item);
            }, () => this.Articles.CurrentPosition >= 0);
            this.CmdItemOpen = new RelayCommand<OxItem>((item) => Messenger.Default.Send(item, Message.Type.OpenItem));
            this.CmdPost = new RelayCommand(() => Extension.Run(() =>
            {
                var item = (OxItem)this.Articles.CurrentItem;
                FacebookConnector.Instance.Post(item);
                TwitterConnector.Instance.Post(item);
            }),
            () =>   this.Articles.CurrentPosition >= 0
                    && ((OxItem)this.Articles.CurrentItem).OxImage.Any());
            this.ReloadLookup();
        }

        private void ReloadLookup()
        {
            this.categorie.Clear();
            this.categorie.Insert(0, new OxCategory());
            MarkOffersConnector.Instance.GetCategorie((result) => result.ForEach(this.categorie.Add));
        }
    }
}
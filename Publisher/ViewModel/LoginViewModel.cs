using System.Net.Mime;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Publisher.Framework;
using Publisher.Framework.Connectors.Mark;

namespace Publisher.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
        public LoginViewModel()
        {
            this.CommandSalva = new RelayCommand<Window>((window) =>
            {
                Properties.Settings.Default.MarkitUsername = this.Indirizzo;
                Properties.Settings.Default.MarkitPassword = this.Password;
                Properties.Settings.Default.Save();
                MarkOffersConnector.Instance.Configure();
                if(MarkOffersConnector.Instance.Configuration.ErrorCode == 0)
                {
                    window.Hide();
                    Messenger.Default.Send(new Message(), Message.Type.OpenMain);
                    window.Close();
                }
                else
                {
                    MessageBox.Show(MarkOffersConnector.Instance.Configuration.ErrorMessage, "Errore", MessageBoxButton.OK);   
                }
            }, (window) =>  string.IsNullOrWhiteSpace(this.Indirizzo) == false && string.IsNullOrWhiteSpace(this.Password) == false);
            this.CommandChiudi = new RelayCommand<Window>((window) =>
            {
                this.Indirizzo = Properties.Settings.Default.MarkitUsername;
                this.Password = Properties.Settings.Default.MarkitPassword;
                window.Close();
            });
            this.Indirizzo = Properties.Settings.Default.MarkitUsername;
            this.Password = Properties.Settings.Default.MarkitPassword;
        }
        public ICommand CommandSalva { get; private set; }
        public ICommand CommandChiudi { get; private set; }

        public string Indirizzo { get; set; }
        public string Password { get; set; }
    }
}
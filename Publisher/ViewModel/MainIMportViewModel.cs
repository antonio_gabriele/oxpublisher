using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Publisher.Core.Model;
//using ServiceStack.OrmLite;

namespace Publisher.ViewModel
{
    public class MainImportViewModel : ViewModelBase
    {
        public ICommand CmdSearch { get; private set; }
        public ICommand CmdItemOpen { get; private set; }
        public ICommand CmdClear { get; private set; }
        /**/
        private readonly ObservableCollection<OxItem> articles = new ObservableCollection<OxItem>();
        /**/
        public ListCollectionView Articles { get; set; }
        /**/
        public MainImportViewModel()
        {
            this.Articles = new ListCollectionView(this.articles);
            this.CmdSearch = new RelayCommand(() =>
            {
                var dialog = new OpenFileDialog();
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    /*
                    this.articles.Clear();
                    var engine = new FileHelperEngine(typeof (Record));
                    try
                    {
                        var records = engine.ReadFile(dialog.FileName) as Record[];
                        foreach (var record in records)
                        {
                            this.articles.Add(Mapper.Map<DataRecord>(record));
                        }
                        if (DataLayer.Instance.Loaded)
                        {
                            using (var connection = DataLayer.Instance.Connection())
                            {
                                var news = Mapper.Map<IEnumerable<DataRecord>>(records);
                                connection.InsertAll(news);
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }*/
                }
            });
            /*
            this.CmdItemOpen = new RelayCommand<DataRecord>((item) =>
            {
                item.Selected = true;
                using (var connection = DataLayer.Instance.Connection())
                {
                    foreach (var record in connection.Where<DataRecord>("Code", item.Code))
                    {
                        record.Selected = true;
                        connection.Update(record);
                    }
                }
                Messenger.Default.Send(Mapper.Map<Item>(item), Message.Type.OpenItem);
            });
            this.CmdClear = new RelayCommand(() =>
            {
                using (var connection = DataLayer.Instance.Connection())
                {
                    connection.DeleteAll<DataRecord>();
                }
                this.articles.Clear();
            }, () => DataLayer.Instance.Loaded && this.articles.Count > 0);
            DataLayer.Instance.Load();
            if (DataLayer.Instance.Loaded)
            {
                using (var connection = DataLayer.Instance.Connection())
                { 
                    connection.Select<DataRecord>().ForEach(this.articles.Add);
                }
            }*/
        }
    }
}
﻿using System;
using Bootstrap.Extensions.StartupTasks;
using GalaSoft.MvvmLight.Messaging;
using Publisher.Framework;
using Publisher.View;
using Publisher.ViewModel;

namespace Publisher.Controller
{
    public class WindowManager : IStartupTask
    {
        public void Run()
        {
            Messenger.Default.Register<Message>(this, Message.Type.OpenMain, (message) =>
            {
                var window = new MainWindow();
                // Attenzione modifica UNICA. 
                //Messenger.Default.Send(new Message(), Message.Type.PrepareNew);
                window.ShowDialog();
            });
            Messenger.Default.Register<Message>(this, Message.Type.OpenCreateCategory, (message) =>
            {
                var window = new CreateCategoryWindow();
                window.ShowDialog();
            });
            Messenger.Default.Register<Message<string,string>>(this, Message.Type.OpenEditor, (message) =>
            {
                var window = new RTFEditorWindow();
                var vm = (RTFEditorModel) window.DataContext;
                vm.Data = message.Data;
                vm.Exit = message.Exit;
                window.ShowDialog();
            });
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}

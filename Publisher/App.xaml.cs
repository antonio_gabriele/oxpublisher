﻿using Bootstrap;
using Bootstrap.Extensions.StartupTasks;
using GalaSoft.MvvmLight.Threading;
using Publisher.Core.Framework;

namespace Publisher
{
    public partial class App
    {
        App()
        {
            Service.Instance.Init();
            DispatcherHelper.Initialize();
            Bootstrapper.With.StartupTasks().Start();
        }
    }
}

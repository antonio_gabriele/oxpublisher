﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using Publisher.Core.Model;

namespace Publisher.Framework.Converters
{
    public class PictureToImageSourceConverter : IValueConverter
    {
        public static IValueConverter Instance = new PictureToImageSourceConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var picture = value as OxImage;
            if (picture == null)
                return null;
            return string.IsNullOrWhiteSpace(picture.DefaultPictureURL) == false ? new BitmapImage(new Uri(picture.DefaultPictureURL)) : new BitmapImage(new Uri("pack://application:,,,/Resources/None.jpg"));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}

﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Publisher.Framework.Connectors;

namespace Publisher.Framework.Converters
{
    public class SocialLoggedToCollapsedConverter : IValueConverter
    {
        public static IValueConverter Instance = new SocialLoggedToCollapsedConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var picture = (Connector.Mode)value;
            if (picture == Connector.Mode.Logged)
                return Visibility.Collapsed;
            else
                return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
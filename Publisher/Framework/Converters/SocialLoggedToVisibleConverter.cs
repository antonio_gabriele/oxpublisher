﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Publisher.Framework.Connectors;

namespace Publisher.Framework.Converters
{
    public class SocialLoggedToVisibleConverter : IValueConverter
    {
        public static IValueConverter Instance = new SocialLoggedToVisibleConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var picture = (Connector.Mode)value;
            return picture == Connector.Mode.Logged ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
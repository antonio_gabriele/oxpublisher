﻿using System.Collections;
using System.Windows.Controls;

namespace Publisher.Framework
{
    public class SelectedItemsBehavior
    {
        private readonly ListBox _listBox;
        private readonly IList _boundList;

        public SelectedItemsBehavior(ListBox listBox, IList boundList)
        {
            this._boundList = boundList;
            this._listBox = listBox;
            this._listBox.SelectionChanged += this.OnSelectionChanged;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this._boundList.Clear();

            foreach (var item in this._listBox.SelectedItems)
            {
                this._boundList.Add(item);
            }
        }
    }
}
﻿using System.ComponentModel;
using GalaSoft.MvvmLight.Threading;
using LinqToTwitter;

namespace Publisher.Framework.Connectors.Twitter
{
    public partial class OAuthPopupTwitter : INotifyPropertyChanged
    {
        public PinAuthorizer Auth;

        public OAuthPopupTwitter()
        {
            this.InitializeComponent();
            this.Web.DocumentCompleted += (sender, args) =>
            {
                var elements = this.Web.Document.GetElementsByTagName("code");
                if (elements.Count > 0)
                {
                    var task = Auth.CompleteAuthorizeAsync(elements[0].InnerText);
                    task.ContinueWith(task1 => DispatcherHelper.CheckBeginInvokeOnUI(this.Close));
                }
            };
            this.Auth = new PinAuthorizer
            {
                CredentialStore = new InMemoryCredentialStore()
                {
                    ConsumerKey = "0UwxpMT3wpFsgOoD0aiSjg",
                    ConsumerSecret = "MHv7uL6vHoJOIzouPk6T8OVKvWIYAWqEs4sJ1QWMMM"
                },
                GoToTwitterAuthorization = pageLink => DispatcherHelper.CheckBeginInvokeOnUI(() => this.Web.Navigate(pageLink))
                
            };   
            this.Auth.BeginAuthorizeAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using LinqToTwitter;
using NLog;
using Publisher.Core.Model;

namespace Publisher.Framework.Connectors.Twitter
{
    public class TwitterConnector : INotifyPropertyChanged, Connector.ISocial
    {

        public bool Enabled { get; set; }

        public static TwitterConnector Instance = new TwitterConnector();

        private TwitterConnector()
        {
            this.CommandLogin = new RelayCommand(() => Instance.Login(), () => Instance.Status == Connector.Mode.None);
            this.CommandExit = new RelayCommand(() => Instance.Logout(), () => Instance.Status == Connector.Mode.Logged || Instance.Status == Connector.Mode.Login);
            this.Profiles = new ObservableCollection<Target>();
        }

        Connector.Mode Connector.ISocial.Status { get; set; }

        private TwitterContext Context;

        public void Login()
        {
            try
            {
                this.Status = Connector.Mode.Login;
                var popup = new OAuthPopupTwitter();
                popup.ShowDialog();
                var authorizer = popup.Auth;
                //if (authorizer.)
                //{
                    this.Status = Connector.Mode.Logged;
                    this.Context = new TwitterContext(authorizer);
                    this.Enabled = true;
                //}
                //else
                //{
                    this.Status = Connector.Mode.None;
                //}
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e.Message);
                this.Status = Connector.Mode.None;
            }
        }

        public ObservableCollection<Target> Profiles { get; set; }

        public Connector.Mode Status { get; private set; }

        public void Logout()
        {
            this.Status  = Connector.Mode.None;
        }

        public void Post(OxItem item)
        {
            if (this.Enabled == false)
                return;
            try
            {
                //this.Context.UpdateAccountProfileAsync(item.Caption);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error(e.Message);
            }            
        }

        public Target Profile { get; set; }
        public ICommand CommandLogin { get; set; }
        public ICommand CommandExit { get; set; }
        
        public event PropertyChangedEventHandler PropertyChanged;
    }
}

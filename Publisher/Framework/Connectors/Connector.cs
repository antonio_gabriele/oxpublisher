﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Publisher.Core.Model;
using Publisher.Framework.Connectors.Mark;

namespace Publisher.Framework.Connectors
{
    public class Connector
    {
        public interface IPortal
        {
            bool Enabled { get; set; }
            Mode Status { get; set; }

            void Login();

            void Logout();

            ICommand CommandLogin { get; set; }
            ICommand CommandExit { get; set; }

            void GetCategorie(Action<IEnumerable<OxCategory>> fetch);

            void GetArticolo(MarkQuery query, Action<List<OxItem>> fetch);

            string GetImage(string reference);

            OxItem SetArticolo(OxItem item);

            void Delete(OxItem item);

            void CreateCategory(string caption);
        }

        public interface ISocial
        {
            bool Enabled { get; set; }

            ObservableCollection<Target> Profiles { get; set; }

            Mode Status { get; set; }

            void Login();

            void Logout();

            void Post(OxItem item);

            Target Profile { get; set; }

            ICommand CommandLogin { get; set; }

            ICommand CommandExit { get; set; }
        }

        public enum Mode
        {
            None,
            Login,
            Logged
        }

    }
}
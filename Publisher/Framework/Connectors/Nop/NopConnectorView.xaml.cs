﻿using System.Windows;

namespace Publisher.Framework.Connectors.Nop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class NopConnectorView : Window
    {
        public NopConnectorView()
        {
            this.InitializeComponent();
        }
    }
}

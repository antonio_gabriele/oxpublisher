﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Publisher.Core.Model;
using Publisher.Nop;

namespace Publisher.Framework.Connectors.Nop
{
    public class NopConnector : INotifyPropertyChanged, Connector.ISocial
    {
        public static readonly NopConnector Instance = new NopConnector();

        public string WebSite { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        private NopServiceClient Nop;
        private NopConnector()
        {
            this.Profiles = new ObservableCollection<Target>();
            AutoMapper.Mapper.CreateMap<Publisher.Nop.Category, Target>()
                .ForMember(d => d.Title, s => s.MapFrom(ss => ss.Caption));

            this.CommandLogin = new RelayCommand(() => Instance.Login(), () => Instance.Status == Connector.Mode.None);
            this.CommandExit = new RelayCommand(() => Instance.Logout(), () => Instance.Status == Connector.Mode.Logged || Instance.Status == Connector.Mode.Login);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Enabled { get; set; }
        public ObservableCollection<Target> Profiles { get; set; }
        public Connector.Mode Status { get; set; }

        public void Login()
        {
            var window = new NopConnectorView();
            window.ShowDialog();
            try
            {
                this.Nop = new NopServiceClient();
                this.Profiles.Clear();
                var categories = this.Nop.GetAllCategories(this.Username, this.Password).ToList();
                categories.ForEach(c => this.Profiles.Add(AutoMapper.Mapper.Map<Target>(c)));
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public void Post(OxItem item)
        {
            throw new NotImplementedException();
        }

        public Target Profile { get; set; }

        public ICommand CommandLogin { get; set; }
        public ICommand CommandExit { get; set; }
    }
}

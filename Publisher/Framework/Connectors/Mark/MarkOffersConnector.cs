﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight.Threading;
using Publisher.Core.Framework;
using Publisher.Core.Model;
using Publisher.MarkOffers;

namespace Publisher.Framework.Connectors.Mark
{
    public class MarkOffersConnector : Connector.IPortal
    {
        public static readonly MarkOffersConnector Instance = new MarkOffersConnector();

        private readonly List<OxCategory> categories = new List<OxCategory>();

        public MOClientResponseOfMO_CONFIGURATION Configuration { get; set; }

        public MarkOffersConnector()
        {
            this.Configure();
        }
        
        private MOServiceSoapClient Create()
        {
            return new MOServiceSoapClient();
        }

        public void Configure()
        {
            var context = this.Create();
            this.Configuration = context.LogonUser(
                Properties.Settings.Default.MarkitUsername,
                Properties.Settings.Default.MarkitPassword,
                Properties.Settings.Default.MarkitIMEI,
                Properties.Settings.Default.MarkitLanguageCode, 
                "CALIO", "CALIO", "1", null, null, true,
                Properties.Settings.Default.MarkitSession);
        }

        public bool Enabled { get; set; }
        public Connector.Mode Status { get; set; }

        public void Login()
        {
            throw new NotImplementedException();
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public ICommand CommandLogin { get; set; }
        public ICommand CommandExit { get; set; }

        public void GetCategorie(Action<IEnumerable<OxCategory>> action)
        {
            var resultset = this.Create().GetKindItem(
                Properties.Settings.Default.MarkitUsername,
                Properties.Settings.Default.MarkitPassword,
                Properties.Settings.Default.MarkitIMEI,
                Properties.Settings.Default.MarkitLanguageCode,
                this.Configuration.Data.First().Vendor.Id, -1, null,
                Properties.Settings.Default.MarkitSession);
            var cats = AutoMapper.Mapper.Map<OxCategory[]>(resultset.Data);
            var list = new List<OxCategory>();
            list.AddRange(cats);
            list.AddRange(this.categories.Except(cats, CategoryComparer.Instance));
            action(list);
        }

        public void GetArticolo(MarkQuery query, Action<List<OxItem>> fetch)
        {
            Extension.Run(() =>
            {
                var lastResultKey = query.LastResultKey;
                do
                {
                    var resultset = this.Create().GetArticolo(
                    Properties.Settings.Default.MarkitUsername,
                    Properties.Settings.Default.MarkitPassword,
                    Properties.Settings.Default.MarkitIMEI,
                    Properties.Settings.Default.MarkitLanguageCode,
                    query.Idarticolo,
                    this.Configuration.Data.First().Vendor.Id,
                    255,
                    null,
                    null,
                    query.KindCode,
                    query.Descrizione,
                    query.ArticoliImportanti,
                    false,
                    false,
                    lastResultKey,
                    Properties.Settings.Default.MarkitSession
                    );
                    DispatcherHelper.CheckBeginInvokeOnUI(() => fetch(AutoMapper.Mapper.Map<List<OxItem>>(resultset.Data)));
                    lastResultKey = resultset.LastResultKey;
                } while (string.IsNullOrWhiteSpace(lastResultKey) == false);
            });
        }

        public string GetImage(string reference)
        {
            return reference.StartsWith("http") ? null : Image.FromFile(reference).Base64();
        }

        public OxItem SetArticolo(OxItem item)
        {
            string image = null;
            if (item.OxImage.Any())
            {
                image = this.GetImage(item.OxImage.First().DefaultPictureURL);
            }
            var resultset = this.Create().SetArticolo(
            Properties.Settings.Default.MarkitUsername,
            Properties.Settings.Default.MarkitPassword,
            Properties.Settings.Default.MarkitIMEI,
            Properties.Settings.Default.MarkitLanguageCode,
            item.Id,
            this.Configuration.Data.First().Vendor.Id,
            255,
            null,
            null,
            null,
            item.Kind,
            item.Caption,
            item.Description,
            item.Price,
            item.OriginalPrice,
            null,
            item.Important,
            false,
            image,
            Properties.Settings.Default.MarkitSession
            );
            item.Id = item.Id == 0 ? resultset.Data.First().Id : item.Id;
            foreach (var picture in item.OxImage.Where(it => (it.Deleted || it.Modified) && it.Index > 0))
            {
                this.Create().SetImage(
                Properties.Settings.Default.MarkitUsername,
                Properties.Settings.Default.MarkitPassword,
                Properties.Settings.Default.MarkitIMEI,
                Properties.Settings.Default.MarkitLanguageCode,
                picture.Id, item.Id,
                'i', true, null, Properties.Settings.Default.MarkitSession);
                picture.Id = 0;
            }
            foreach (var picture in item.OxImage.Where(it => it.Modified && it.Index > 0))
            {
                var container = Image.FromFile(picture.DefaultPictureURL);
                image = container.Base64();
                this.Create().SetImage(
                Properties.Settings.Default.MarkitUsername,
                Properties.Settings.Default.MarkitPassword,
                Properties.Settings.Default.MarkitIMEI,
                Properties.Settings.Default.MarkitLanguageCode,
                picture.Id, item.Id,
                'i', false, image, Properties.Settings.Default.MarkitSession);
            }
            return AutoMapper.Mapper.Map<OxItem>(resultset.Data.First());
        }
        public void Delete(OxItem item)
        {
            this.Create().SetArticolo(
            Properties.Settings.Default.MarkitUsername,
            Properties.Settings.Default.MarkitPassword,
            Properties.Settings.Default.MarkitIMEI,
            Properties.Settings.Default.MarkitLanguageCode,
            (item.Id == Int32.MaxValue ? null : (int?)item.Id),
            this.Configuration.Data.First().Vendor.Id,
            255, null, null, null,
            item.Kind,
            item.Caption,
            item.Description,
            item.Price,
            item.OriginalPrice,
            null, false, true, null,
            Properties.Settings.Default.MarkitSession);
        }

        public void CreateCategory(string caption)
        {
            this.categories.Add(new OxCategory() { Caption = caption });
        }
    }
}

﻿using System.ComponentModel;

namespace Publisher.Framework.Connectors.Mark
{
    public class MarkQuery : INotifyPropertyChanged
    {
        public int VendorId { get; set; }
        public bool? ArticoliImportanti { get; set; }
        public bool? Expired { get; set; }
        public int? Idarticolo { get; set; }
        public string KindCode { get; set; }
        public string Descrizione { get; set; }
        public string LastResultKey { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
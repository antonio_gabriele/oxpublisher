﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Windows.Input;
using Facebook;
using GalaSoft.MvvmLight.Command;
using Publisher.Core.Model;
using Publisher.Properties;

namespace Publisher.Framework.Connectors.Facebook
{

    public class FacebookConnector : INotifyPropertyChanged, Connector.ISocial
    {
        public ObservableCollection<Target> Profiles { get; set; }

        public Connector.Mode Status { get; set; }

        public bool Enabled { get; set; }

        public Target Profile { get; set; }

        public static FacebookConnector Instance = new FacebookConnector();

        public ICommand CommandLogin { get; set; }
        public ICommand CommandExit { get; set; }

        private FacebookConnector()
        {
            this.CommandLogin = new RelayCommand(() => Instance.Login(), () => Instance.Status == Connector.Mode.None);
            this.CommandExit = new RelayCommand(() => Instance.Logout(), () => Instance.Status == Connector.Mode.Logged || Instance.Status == Connector.Mode.Login);
            this.Profiles = new ObservableCollection<Target>();
            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.FacebookToken) == false)
            {
                this.TryLogin(Properties.Settings.Default.FacebookToken);
            }
        }
        
        public void Login()
        {
            this.Status = Connector.Mode.Login;
            var popup = new OAuthPopupFacebook();
            popup.ShowDialog();
            this.TryLogin(popup.Client.AccessToken);
        }

        private void TryLogin(String token)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(token) == false)
                {
                    var client = new FacebookClient(token);
                    this.Status = Connector.Mode.Logged;
                    this.Profiles.Clear();
                    this.Profiles.Add(new Target {AccessToken = token, Path = "me/feed", Title = "Profilo"});
                    var jsonResponse = client.Get("me/accounts") as JsonObject;
                    foreach (var account in (JsonArray) jsonResponse["data"])
                    {
                        this.Profiles.Add(new Target()
                        {
                            AccessToken = (string) (((JsonObject) account)["access_token"]),
                            Path = string.Format("{0}/feed", (string) (((JsonObject) account)["id"])),
                            Title = (string) (((JsonObject) account)["name"])
                        });
                    }
                    this.Profile = this.Profiles.FirstOrDefault();
                    this.Enabled = true;
                    Properties.Settings.Default.FacebookToken = token;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    this.Status = Connector.Mode.None;
                }
            }
            catch (FacebookOAuthException e)
            {
                if (e.ErrorCode != 190)
                {
                    NLog.LogManager.GetCurrentClassLogger().Error(e.Message);                    
                }
                this.Status = Connector.Mode.None;
            }
            catch (Exception e)
            {
                this.Status = Connector.Mode.None;
            }
        }

        public void Logout()
        {
            this.Status = Connector.Mode.None;
        }

        public void Post(OxItem item)
        {
            if(this.Enabled == false)
                return;
            var client = new FacebookClient(this.Profile.AccessToken);
            dynamic messagePost = new ExpandoObject();
            messagePost.picture = item.OxImage.First().DefaultPictureURL;
            messagePost.name = item.Caption;
            messagePost.caption = item.Kind;
            messagePost.description = item.Description;
            messagePost.message = item.Description;
            try
            {
                client.Post(this.Profile.Path, messagePost);
            }
            catch (FacebookOAuthException ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(ex.Message);
            }
            catch (FacebookApiException ex)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(ex.Message);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}

﻿using System;
using System.Dynamic;
using Facebook;

namespace Publisher.Framework.Connectors.Facebook
{
    public partial class OAuthPopupFacebook
    {
        public FacebookClient Client = new FacebookClient();
        public OAuthPopupFacebook()
        {
            this.InitializeComponent();
            this.Web.Navigated += (sender, args) =>
            {
                if (FacebookConnector.Instance.Status == Connector.Mode.Login)
                {
                    FacebookOAuthResult result;
                    if (this.Client.TryParseOAuthCallbackUrl(new Uri(args.Url.ToString()), out result))
                    {
                        if (result.IsSuccess)
                        {
                            this.Client.AccessToken = result.AccessToken;
                            this.Close();
                        }
                    }
                }
            };
            dynamic parameters = new ExpandoObject();
            parameters.client_id = Properties.Settings.Default.FacebookApiKey;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";
            parameters.response_type = "token";
            parameters.display = "popup";
            parameters.scope = Properties.Settings.Default.FacebookPermissions;
            this.Web.Navigate(this.Client.GetLoginUrl(parameters));
        }
    }
}

﻿using System.Collections.Generic;

namespace Publisher.Framework
{
    public class AddOns
    {
        public static readonly IDictionary<string, bool?> SiNoNull = new Dictionary<string, bool?>()
        {
            {"Si", true},{"No", false},{"Tutti", null}
        };

        public static readonly IDictionary<string, bool?> SiNo = new Dictionary<string, bool?>()
        {
            {"Si", true},{"No", false}
        };
    }
}
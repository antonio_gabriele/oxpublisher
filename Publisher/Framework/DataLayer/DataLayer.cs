﻿using System.Data;
using ServiceStack.OrmLite;

namespace Publisher.Framework.DataLayer
{
    public class DataLayer
    {
        private OrmLiteConnectionFactory factory;
        public static readonly DataLayer Instance = new DataLayer();
        public bool Loaded { get; set; }
        public IDbConnection Connection()
        {
            return this.factory.OpenDbConnection();
        }
        public void Load()
        {
            this.factory = new OrmLiteConnectionFactory(Properties.Settings.Default.Sqlite, SqliteDialect.Provider);
            using (var db = this.factory.OpenDbConnection())
            {
                db.CreateTable<DataRecord>(false);
                this.Loaded = true;
            }
        }
    }
}
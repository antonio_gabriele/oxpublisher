﻿using System.ComponentModel;

namespace Publisher.Framework.DataLayer
{
    public class DataRecord: INotifyPropertyChanged
    {
        public int? Key { get; set; }
        public int? mId { get; set; }
        public string Code { get; set; }
        public bool Selected { get; set; }
        public string Caption { get; set; }
        public string Kind { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
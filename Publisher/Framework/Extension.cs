﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.Messaging;

namespace Publisher.Framework
{
    public static class Extension
    {
        public static void Run(Action what)
        {
            var task = new Task(() =>
            {
                Messenger.Default.Send(new Message(), Message.Type.Busy);
                what();
                Messenger.Default.Send(new Message(), Message.Type.Ready);
            });
            task.Start();
        }

        public static void Execute(this ICommand command)
        {
            command.Execute(null);
        }


        public static void CopyTo<T>(this T src, T dst)
        {
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(src))
            {
                property.SetValue(dst, property.GetValue(src));
            }
        }

        public static string PictureTempName()
        {
            return System.IO.Path.GetTempPath() + "\\" + Guid.NewGuid().ToString().Replace("-", "") + ".png";
        }

        public static string PictureName(int index)
        {
            return index == 0 ? Properties.Resources.Text_Foto_Principale : Properties.Resources.Text_Foto + " " + index;
        }

        public static Image LoadImageFromIMGUrl(string url)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            using (var httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                using (var stream = httpWebReponse.GetResponseStream())
                {
                    return Image.FromStream(stream);
                }
            }
        }

        public static Image LoadImageFromIMGTag(string src, string basepath)
        {
            Image image;
            if (src.StartsWith("data:"))
            {
                image = LoadImageFromIMGBase64(src);
            }
            else
            {
                if (src.StartsWith("http") == false)
                {
                    var path = basepath.Substring(1, basepath.LastIndexOf('/'));
                    src = path + "src";
                }
                image = LoadImageFromIMGUrl(src);
            }
            return image;
        }
        /*
        public static ImageSource GetImageSourceFromImage(Image image)
        {
            var stream = new MemoryStream();
            image.Save(stream, ImageFormat.Png);
            var decoder = new PngBitmapDecoder(stream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            return decoder.Frames[0];
        }
        */

        public static Image LoadImageFromIMGBase64(string src)
        {
            var token = src.Split(',');
            src = token.Length > 0 ? token[1] : token[0];
            var stream64 = Convert.FromBase64String(src);
            using (var stream = new MemoryStream(stream64))
            {
                return Image.FromStream(stream);
            }
        }
        /*
        public static string GetBase64FromImageSource(ImageSource source)
        {
            using (var stream = new MemoryStream())
            {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add((BitmapFrame)source);
                encoder.Save(stream);
                return Convert.ToBase64String(stream.GetBuffer());
            }
        }
        */
        public static string Base64(this Image source)
        {
            using (var stream = new MemoryStream())
            {
                source.Save(stream, ImageFormat.Png);
                return Convert.ToBase64String(stream.GetBuffer());
            }
        }

        public static Image GetThumbnailImageX(this Image image)
        {
            const int square = 300;
            var top = 0;
            var left = 0;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            nPercentW = (square / (float)image.Width);
            nPercentH = (square / (float)image.Height);
            nPercent = nPercentH < nPercentW ? nPercentH : nPercentW;
            var destWidth = (int)(image.Width * nPercent);
            var destHeight = (int)(image.Height * nPercent);
            var destination = new Bitmap(square, square);
            using (var graphics = Graphics.FromImage(destination))
            {
                if (destWidth < square)
                {
                    left = (square - destWidth) / 2;
                }
                if (destHeight < square)
                {
                    top = (square - destHeight) / 2;
                }
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(image, left, top, destWidth, destHeight);
                return destination;
            }
        }

    }
}
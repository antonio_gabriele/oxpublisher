﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Publisher.Framework
{
    public static class ExpanderHelper
    {
        public static readonly DependencyProperty SyncProperty = DependencyProperty.RegisterAttached("Sync", typeof(bool), typeof(ExpanderHelper), new FrameworkPropertyMetadata(false, OnSyncPropertyChanged));

        public static bool GetSync(DependencyObject dp)
        {
            return (bool)dp.GetValue(SyncProperty);
        }

        public static void SetSync(DependencyObject dp, bool value)
        {
            dp.SetValue(SyncProperty, value);
        }

        private static void OnSyncPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var panel = sender as Panel;
            panel.Loaded += (o, args) =>
            {
                var container = (Panel)o;
                var elements = new List<Expander>();
                elements.AddRange(container.Children.OfType<Expander>());
                elements.ForEach(current =>
                {
                    current.Expanded += (sender1, eventArgs) =>
                    {
                        foreach (var expander in elements.Where(it => it != current))
                        {
                            expander.IsExpanded = false;
                        }
                    };
                });
            };
        }
    }
}
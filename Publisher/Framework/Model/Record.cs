﻿using FileHelpers;

namespace Publisher.Framework.Model
{
    [DelimitedRecord(";")]
    public class Record
    {
        public string Code
        {
            get { return this.code; }
            set { this.code = value; }
        }
        public string Kind
        {
            get { return this.kind; }
            set { this.kind = value; }
        }
        public string Caption
        {
            get { return this.caption; }
            set { this.caption = value; }
        }
        public decimal? Price
        {
            get { return this.price; }
            set { this.price = value; }
        }
        /**/
        [FieldTrim(TrimMode.Both)]
        private string code;
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        private string kind;
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        private string caption;
        [FieldOptional]
        [FieldTrim(TrimMode.Both)]
        private decimal? price;

    }
}

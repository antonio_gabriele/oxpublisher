﻿using System;

namespace Publisher.Framework
{
    public class Message
    {
        public enum Type
        {
            OpenItem,
            OpenMain,
            OpenConfigure,
            OpenEditor,
            OpenCreateCategory,
            ReloadConfiguration,
            ReloadLookup,
            PrepareNew,
            Busy,
            Ready
        }
    }
    public class Message<TP, TQ> : Message
    {
        public TP Data { get; set; }
        public Action<TQ> Exit { get; set; }
    }
}

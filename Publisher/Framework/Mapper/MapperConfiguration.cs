﻿using System;
using Bootstrap.Extensions.StartupTasks;
using Publisher.Core.Model;
using Publisher.MarkOffers;

namespace Publisher.Framework.Mapper
{
    public class MapperConfiguration : IStartupTask
    {
        public void Run()
        {
            AutoMapper.Mapper.CreateMap<OxItem, MO_ITEM>()
                .ForMember(d => d.OtherArticleImages, s => s.MapFrom(ss => ss.OxImage));
            AutoMapper.Mapper.CreateMap<MO_ITEM, OxItem>()
                      .ForMember(d => d.OxImage, s => s.MapFrom(ss => ss.OtherArticleImages))
                      .AfterMap((s, d) =>
                      {
                          d.OxImage.Add(new OxImage()
                          {
                              Id = 0,
                              DefaultPictureURL = s.DefaultPictureURL
                          });
                          int i = 0;
                          foreach (var image in d.OxImage)
                          {
                              image.Index = i++;
                              image.Name = Extension.PictureName(image.Index);
                              image.DefaultPictureURL = image.DefaultPictureURL.Replace("http://storage.markoffers.com/imagesitem/", "http://markoffers.blob.core.windows.net/images/item/");
                          }
                      });

            AutoMapper.Mapper.CreateMap<OxCategory, MO_ENUM_KIND_ITEM>()
                .ForMember(d => d.Caption, s => s.MapFrom(ss => ss.Caption));
            AutoMapper.Mapper.CreateMap<MO_ENUM_KIND_ITEM, OxCategory>()
                .ForMember(d => d.Caption, s => s.MapFrom(ss => ss.Caption));
            AutoMapper.Mapper.CreateMap<OxImage, MO_IMAGE>();
            AutoMapper.Mapper.CreateMap<MO_IMAGE, OxImage>();
            //AutoMapper.Mapper.CreateMap<DataRecord, Item>();
            //AutoMapper.Mapper.CreateMap<Item, DataRecord>();
            //AutoMapper.Mapper.CreateMap<Record, DataRecord>();
            //AutoMapper.Mapper.CreateMap<DataRecord, Record>();
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}

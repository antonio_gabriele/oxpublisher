﻿using System.Collections.Generic;
using Publisher.Core.Model;

namespace Publisher.Core.Framework
{
    public class CategoryComparer : IEqualityComparer<OxCategory>
    {
        public static readonly IEqualityComparer<OxCategory> Instance = new CategoryComparer();
        public bool Equals(OxCategory x, OxCategory y)
        {
            return x.Caption == y.Caption;
        }

        public int GetHashCode(OxCategory obj)
        {
            return obj.GetHashCode();
        }
    }
}

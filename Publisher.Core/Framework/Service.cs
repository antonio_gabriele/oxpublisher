﻿using System;
using System.Linq;
using Publisher.Core.Model;

namespace Publisher.Core.Framework
{
    public class Service
    {
        public static readonly Service Instance = new Service();
        public bool Init()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
            using (var entities = new Ox())
            {
                entities.Database.CreateIfNotExists();
                if (entities.Database.CompatibleWithModel(false) == false)
                {
                    entities.Database.Delete();
                    entities.Database.Create();
                }
                var t = entities.OxItemSet.ToList();
            }
            return true;
        }
    }
}

﻿namespace Publisher.Core.Model
{
    public class Target
    {
        public string AccessToken { get; set; }
        public string Path { get; set; }
        public string Title { get; set; }
    }
}
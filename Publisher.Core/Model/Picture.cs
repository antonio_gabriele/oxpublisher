﻿using System.ComponentModel;
using System.Drawing;

namespace Publisher.Core.Model
{
    public class Picture : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string DefaultPictureURL { get; set; }
        public int Index { get; set; }
        public long? Id { get; set; }
        public Image Image { get; set; }
        public bool Modified { get; set; }
        public bool Deleted { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }

}
﻿using System.ComponentModel;

namespace Publisher.Core.Model
{
    public class Category : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
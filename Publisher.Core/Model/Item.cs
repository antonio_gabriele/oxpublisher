﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Publisher.Core.Model
{
    public class Item : INotifyPropertyChanged
    {
        public Item()
        {
            this.ArticleImages = new ObservableCollection<Picture>();
        }

        public int? Id { get; set; }
        public int VendorId { get; set; }
        public string DefaultPictureURL { get; set; }
        public string Code { get; set; }
        public string BarCode { get; set; }
        public string Kind { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal? OriginalPrice { get; set; }
        public DateTime? ExpiringDate { get; set; }
        public bool Important { get; set; }
        public ObservableCollection<Picture> ArticleImages { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}